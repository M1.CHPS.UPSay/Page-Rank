If you want to use another version of this code (mainly the parallel version) you have to use git to move from this branch to another. The branches available are :
master
MPI_Allgatherv_Synchrone (Synchrone MPI Comm)
MPI_Allgatherv
MPI_Allreduce_Synchrone  (Synchrone MPI Comm)
MPI_Allreduce 

Compilation :

To compile this Project with Cmake you need version 2.6 or higher. The CMakeLists.txt file is located in the src folder.

The Blas library is required. For the parallel versions of the code, a MPI library and OpenMP is required.

You can either Cmake with or use the Makefile situated in the PageRank/src folder.

To use the Makefile in the PageRank/src folder you will probably need to modify it.

Sequential Usage :

You can choose to define yourself the Tolerance value, Damping value (DELTA) and the maximum number of iteration when compiling the program by using the -D cmake option like this :
cmake -DTOL_VALUE=1e-8 -DDELTA_VALUE=0.75 -DMAX_ITE_VALUE=50
The defaults values are 1e-14 for the Tolerance and 0.85 for the Damping and 200 for the maximum number of Iteration.

Our program can be executed without arguments, with one argument or two arguments.

If you execute the program without arguments, the format CSR will be used and a small matrix of dimensions 4 by 4 will be used.

To pass the arguments to the program you can either use pass them directly like this : ./build/Pagerank _Arguments_ 

If you execute the program with only one arguments, it has to be an integer and it allow you to choose between CSR and COO.
 If the integer is 0, COO will be used. CSR will be used in any other case.

If you execute the program with two arguments, the first arguments has to be an integer and allow you to choose between CSR and COO.
 It works exactly as stated above. The second argument must be the absolute or relative path to the matrix you want to use.

Regarding the matrix, it has to be in Matrix Market format in Row Major, if it isn't the program won't work. 
You can use the matrices located in the PageRank/matrix_Row folder but you have to use Git LFS extension to download them.

You can use a matrix in Matrix Market format in Collumn Major by putting '2' as the first argument, the matrix will be converted as it's read, note that this can take some time.
If you can use multiple core or thread it is quicker to convert the matrix beforehand, for that purpose we created a program that convert the matrix and uses an Hybrid MPI+OpenMP implementation.
If you have only one core or thread, reading it directly will be faster (but you have to do it every time).

The Matrix_Converter will be compiled if you use CMAKE. It is used like this : ./build/Matrix_Converter "Input_Matrix" "Output_Matrix".

The Matrix_Converter code is only located on the master branch.


Parallel Usage :

It's the same as Sequential for simply launching the program, but you have to launch with your MPI launcher, and pay attention to your "OMP_NUM_THREADS" environment variable.

You can also enable Weak Scalability testing by changing the definition of the constant "WEAKSCAL" in src/Matrix_Market/matrix_market_read.cpp .