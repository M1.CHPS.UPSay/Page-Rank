#ifndef SPARSE_STRUCT_H
#define SPARSE_STRUCT_H

#include <vector>

struct matSPRS
{
	std::vector<double> Val; //size n, stores 1 value for each columnthe value before damping is the number of '1' in the column
							//After damping it stores the value a '1' in the column would have had
	std::vector<int> Col;    //size nnz
	std::vector<int> Row;    //Size n+1 for CSR, nnz for COO
};



#endif
