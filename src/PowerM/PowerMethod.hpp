#ifndef POWERMETHOD_H
#define POWERMETHOD_H

#include <cmath>
#include <iomanip>
#include <algorithm>
#include <iostream>
extern "C"
{
#include <cblas.h>
}
#include "../include/Sparse_Struct.hpp"
#include "../Bin/Config.hpp"


double powerMethodeCSR(matSPRS A, const int SIZE, double b[],const int ite);   //Power Method for CSR sparse Matrix

double powerMethodeCOORM(matSPRS A, const int SIZE, double b[],const int ite);   //Power Method for COO Row Major sparse Matrix

#endif
