using namespace std;
#include "PowerMethod.hpp"
#include <string.h>

double powerMethodeCSR(matSPRS A, const int SIZE, double b[],const int ite)
{
	double *tmp,*tmp2,*emptyvect; 					//Temporary vectors
	tmp=(double *)malloc(SIZE*sizeof(double)); 		//Result of Matrix-Vector multiplication
	tmp2=(double *)malloc(SIZE*sizeof(double));		//Rows of the Matrix
	emptyvect=(double *)malloc(SIZE*sizeof(double));//Used for faster loading of tmp2
	double norm=0; 		// norm of tmp vector
	bool fini=false;	//true if the eigenvalue didn't change
	int k=0; 			//counter for the number of iteration
	const double colEmpty=(1./SIZE), zeroValue=((1.-DELTA)/SIZE);
	//Creation of base vector before taking account of the 1
	fill_n(emptyvect,SIZE,zeroValue);
	for(int j=0;j<SIZE;j++)
	{
		if(A.Val[j]==colEmpty)
		{
			emptyvect[j]=colEmpty;
		}
	}
	while(k<ite && !fini)
	{
		cout<<"Itération :"<<k+1<<endl;
		for(int i=0; i<SIZE; i++)
		{
			memcpy(tmp2,emptyvect,SIZE*sizeof(double));
			for (int j=A.Row[i]; j<A.Row[i+1]; j++)
			{
				tmp2[A.Col[j]]= A.Val[A.Col[j]];
			}
			tmp[i]=cblas_ddot(SIZE,tmp2,1,b,1);
		}
		
		//calculation of the vector's norm
		norm=cblas_dnrm2(SIZE,tmp,1);
		//Checking tolerance
		if(k>0 && fabs(norm-1)<TOL)
		{
			fini=true;
		}
		fprintf(stdout,"Erreur temporaire : %.14g\n",fabs(norm-1));
		if(k<(ite-1)) //Preparing b for the next iteration
		{
			cblas_dcopy(SIZE,tmp,1,b,1);
			cblas_dscal(SIZE,1/norm,b,1);
		}
		k++;
	}
	cout<<"\n";
	free(tmp);
	free(tmp2);
	free(emptyvect);
	return(norm);
}

double powerMethodeCOORM(matSPRS A, const int SIZE, double b[],const int ite) // The same as powerMethodeCSR but for COO Row Major
{
	double *tmp,*tmp2,*emptyvect; 					//Temporary vectors
	tmp=(double *)malloc(SIZE*sizeof(double)); 		//Result of Matrix-Vector multiplication
	tmp2=(double *)malloc(SIZE*sizeof(double));		//Rows of the Matrix
	emptyvect=(double *)malloc(SIZE*sizeof(double));//Used for faster loading of tmp2
	double norm=0; 				 // La norm de tmp
	int cpt=0;
	bool fini=false;
	int k=0;
	const int RSIZE=A.Row.size();
	const double colEmpty=(1./SIZE), zeroValue=((1.-DELTA)/SIZE);
	//Creation of base vector before taking account of the 1
	fill_n(emptyvect,SIZE,zeroValue);
	for(int j=0;j<SIZE;j++)
	{
		if(A.Val[j]==colEmpty)
		{
			emptyvect[j]=colEmpty;
		}
	}
	while(k<ite && !fini)
	{
		cout<<"Itération :"<<k+1<<endl;
		cpt=0;
		for(int i=0; i<SIZE; i++)
		{
			memcpy(tmp2,emptyvect,SIZE*sizeof(double));
			//The Row are sorted in order, so we use cpt to keep in memory where was the end of a row and the beginning of the next
			while(cpt<RSIZE && A.Row[cpt]==i) //Sparse Dot Product for each value in the Sparse matrix
			{
				tmp2[A.Col[cpt]]= A.Val[A.Col[cpt]];
				cpt++;
			}
			tmp[i]=cblas_ddot(SIZE,tmp2,1,b,1);
		}
		
		//calculation of the vector's norm
		norm=cblas_dnrm2(SIZE,tmp,1);
		//Checking tolerance
		if(k>0 && fabs(norm-1)<TOL)
		{
			fini=true;
		}
		
		fprintf(stdout,"Erreur temporaire : %.14g\n",fabs(norm-1));
		if(k<(ite-1)) //Preparing b for the next iteration
		{
			cblas_dcopy(SIZE,tmp,1,b,1);
			cblas_dscal(SIZE,1/norm,b,1);
		}
		k++;
	}
	cout<<"\n";
	free(tmp);
	free(tmp2);
	free(emptyvect);
	return(norm);
}
