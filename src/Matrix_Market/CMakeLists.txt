add_library(MMIO mm_io.cpp)
add_library(MatrixMarket matrix_market_read.cpp)
target_link_libraries (MatrixMarket MMIO)
